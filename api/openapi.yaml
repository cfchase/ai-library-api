openapi: 3.0.0
info:
  contact:
    email: croberts@redhat.com
  description: This is the API definition for interacting with models served by the
    AI Library.
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  title: AI Library
  version: 0.0.1
servers:
- url: /
tags:
- description: Linear Regression model
  name: Linear Regression
paths:
  /prediction/linear-regression:
    post:
      operationId: lrpredictions
      parameters: []
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LinearRegressionRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LinearRegressionResponse'
          description: predictionResults - json representation of the prediction result
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the linear-regression model
      tags:
      - linear-regression
  /prediction/fraud-detection:
    post:
      operationId: fdpredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/FraudDetectionRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FraudDetectionResponse'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the fraud detection model
      tags:
      - fraud-detection
  /prediction/associative-rule-learning:
    post:
      operationId: arlpredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/AssociativeRuleLearningRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AssociativeRuleLearningResponse'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the associative rule learning model
      tags:
      - associative-rule-learning
  /prediction/correlation-analysis:
    post:
      operationId: capredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/CorrelationAnalysisRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CorrelationAnalysisResponse'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the correlation analysis model
      tags:
      - correlation-analysis
  /prediction/duplicate-bug-predict:
    post:
      operationId: predictdb
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/DuplicateBugPredictRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DuplicateBugPredictResponse'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the duplicate bug model
      tags:
      - duplicate-bug-predict
  /prediction/duplicate-bug-train:
    post:
      operationId: traindb
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/DuplicateBugTrainRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DuplicateBugTrainResponse'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a training request to the duplicate bug model
      tags:
      - duplicate-bug-train
  /prediction/flakes-predict:
    post:
      operationId: pedictflakes
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/FlakesPredictRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FlakesPredictResponse'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the flakes model
      tags:
      - flakes-predict
  /prediction/flakes-train:
    post:
      operationId: trainflakes
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/FlakesTrainRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FlakesTrainResponse'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a training request to the flakes model
      tags:
      - flakes-train
  /prediction/matrix-factorization:
    post:
      operationId: mfpredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/MatrixFactorizationRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MatrixFactorizationResponse'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the matrix factorization model
      tags:
      - matrix-factorization
  /prediction/topic-model:
    post:
      operationId: tmpredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/TopicModelRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TopicModelResponse'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the topic model
      tags:
      - topic-model
components:
  callbacks: {}
  links: {}
  requestBodies: {}
  schemas:
    LinearRegressionRequest:
      example:
        predictionData:
        - 0.8008282
        - 0.8008282
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    LinearRegressionResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
    FraudDetectionRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    FraudDetectionResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
    AssociativeRuleLearningRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    AssociativeRuleLearningResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
    CorrelationAnalysisRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    CorrelationAnalysisResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
    DuplicateBugPredictRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    DuplicateBugPredictResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
    DuplicateBugTrainRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    DuplicateBugTrainResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
    FlakesTrainRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    FlakesTrainResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
    FlakesPredictRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    FlakesPredictResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
    MatrixFactorizationRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    MatrixFactorizationResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
    TopicModelRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    TopicModelResponse:
      example:
        predictionResults: predictionResults
      properties:
        predictionResults:
          type: string
      required:
      - predictionResults
      type: object
  securitySchemes: {}
