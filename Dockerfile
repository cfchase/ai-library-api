FROM golang:1.10
WORKDIR /go/src
COPY go ./go
COPY main.go .

ENV CGO_ENABLED=0
RUN go get -d -v ./...

RUN go build -a -installsuffix cgo -o ailibraryapi .

EXPOSE 8080/tcp
ENTRYPOINT ["./ailibraryapi"]
