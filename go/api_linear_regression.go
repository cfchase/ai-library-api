/*
 * AI Library
 *
 * This is the API definition for interacting with models served by the AI Library.
 *
 * API version: 0.0.1
 * Contact: croberts@redhat.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package ailibraryapi

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/getkin/kin-openapi/openapi3filter"
)

// Predictions - Submit a request to the linear-regression model
func Lrpredictions(w http.ResponseWriter, r *http.Request) {
	ctx := context.TODO()
	rtr := openapi3filter.NewRouter().WithSwaggerFromFile("swagger.yaml")
	route, pathParams, _ := rtr.FindRoute(r.Method, r.URL)
	requestValidationInput := &openapi3filter.RequestValidationInput{
		Request:    r,
		PathParams: pathParams,
		Route:      route,
	}
	if err := openapi3filter.ValidateRequest(ctx, requestValidationInput); err != nil {
		http.Error(w, fmt.Sprintf("%v\n", err), http.StatusBadRequest)
		return
	}

	jsonString, err := LinearRegressionPrediction(w, r)
	if err != nil {
		http.Error(w, fmt.Sprintf("Unable to produce response %v", err), http.StatusBadRequest)
		return
	}

	responseValidationInput := &openapi3filter.ResponseValidationInput{
		RequestValidationInput: requestValidationInput,
		Status:                 200,
		Header: http.Header{
			"Content-Type": []string{
				"application/json; charset=UTF-8",
			},
		},
	}
	jsonResp, _ := json.Marshal(jsonString)
	responseValidationInput.SetBodyBytes(jsonResp)
	if err := openapi3filter.ValidateResponse(ctx, responseValidationInput); err != nil {
		http.Error(w, fmt.Sprintf("%v\n", err), http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(jsonResp)
}
