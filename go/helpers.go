package ailibraryapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// Predictions - Submit a request to the linear-regression model
func LinearRegressionPrediction(w http.ResponseWriter, r *http.Request) (LinearRegressionResponse, error) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("Parse body failed: %v", err), http.StatusBadRequest)
	}
	var predictionRequest LinearRegressionRequest
	err = json.Unmarshal(body, &predictionRequest)
	fmt.Printf("The input data is: %s\n", predictionRequest)
	//s3accesskey := os.Getenv("S3KEY")
	//s3secretkey := os.Getenv("S3SECRET")
	//s3Endpoint := os.Getenv("S3ENDPOINT")
	//linearRegressionEndpoint := os.Getenv("LRENDPOINT")
	//reqBody := "{strData: \"s3endpointUrl=" + s3Endpoint + ", s3accessKey=" + s3accesskey + ", s3secretKey=" + s3secretkey +
	//	", s3objectStoreLocation=DH-DEV-DATA" +
	//	", s3Path=risk_analysis/" +
	//	", s3Destination=risk_analysis/results" +
	//	", training=training.csv" +
	//	", prediction=prediction.csv\"}"
	//http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	//// TODO:  get a real response here and use it below
	//_, err := http.Post(linearRegressionEndpoint, "application/json", strings.NewReader(reqBody))
	////fmt.Printf("The response is: %v", resp)
	//if err != nil {
	//	http.Error(w, fmt.Sprintf("Problem sending request to model: %s", err), http.StatusInternalServerError)
	//}

	jsonResp := LinearRegressionResponse{PredictionResults: "[0.0,1,0,0,0,1]"}
	// TODO use actual error
	return jsonResp, nil
}
